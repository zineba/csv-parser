#
# Variables par défaut (surcharge)
#
CXXFLAGS = -std=c++11 -Wall -g -O0
#
# Variables utilisateur
#
SRC = CSVParser.cpp CSVParserTest.cpp Person.cpp PersonTest.cpp \
	Fraction.cpp FractionTest.cpp
#INC = CSVParser.hpp
OBJ = $(SRC:.cpp=.o)

%: %.o
	$(LINK.cc) -o $@ $^

test: $(OBJ)
	g++ -o $@ $^

CSVParser.o: CSVParser.cpp CSVParser.hpp
CSVParserTest.o: CSVParserTest.cpp CSVParser.hpp catch.hpp
Person.o: Person.cpp Person.hpp
PersonTest.o: PersonTest.cpp Person.hpp catch.hpp
Fraction.o: Fraction.cpp Fraction.hpp 
FractionTest.o: FractionTest.cpp Fraction.hpp catch.hpp

#
# Cibles habituelles
#
cleaner: clean
	rm -f test

clean:
	rm -f *.o

