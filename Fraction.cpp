/*
 * Fraction.cpp
 *
 */

#include "Fraction.hpp"
#include <iostream>
 
using namespace std;


/* Fonction interne */
bool HasNullDenominator(Fraction a) {
    if (a.denominator == 0) {
        cerr << "Erreur: dénominateur nul." << endl;
        return true;
    }
    return false;
}

/* Fonction membre */
string Fraction::display() {
    if (HasNullDenominator(*this)) {
        // vérifie que le dénominateur est non nul
        return ""; // string vide si vrai
    }
    
    return to_string(this->numerator) + "/" + to_string(this->denominator);
}

/* Fonction membre */
void Fraction::operator+(int i) {
    // vérifie que le dénominateur est non nul
    if (!HasNullDenominator(*this)) {
        this->numerator = this->numerator + i*this->denominator;
    }
    // ne fait rien sinon
}

/* Fonction membre */
void Fraction::operator+(const Fraction a) {
    // Si un des dénominateurs est nul, on ne fait rien
    if (HasNullDenominator(a) || HasNullDenominator(*this) )
        return;
    
    this->numerator = 
        this->numerator * a.denominator + a.numerator * this->denominator;
    this->denominator = this->denominator * a.denominator;
}

/* Fonction membre */
bool Fraction::operator<(const Fraction a) {
    // Par défaut la fraction valide est supérieure
    if (HasNullDenominator(a))
        return false;
     if (HasNullDenominator(*this))
         return true;
    
    // variables temporaires pour obtenir le quotient à virgule
    float j = this->numerator;
    j = j / this->denominator;
    float k = a.numerator;
    k = k / a.denominator;
    // sinon comparer les fractions
    return j < k;
}

/* Fonction membre */
bool Fraction::operator>(const Fraction a) {
    // Par défaut la fraction valide est supérieure
    if (HasNullDenominator(a))
        return true;
     if (HasNullDenominator(*this))
         return false;
    
    // variables temporaires pour obtenir le quotient à virgule
    float j = this->numerator;
    j = j / this->denominator;
    float k = a.numerator;
    k = k / a.denominator;
    // sinon comparer les fractions
    return j > k;
}
