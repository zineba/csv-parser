#include "Person.hpp"
#include <cctype>
#include <cwctype>
#include <clocale>

using namespace std;

/* 
* Fonction membre pour valider les numéros de téléphone
*/
bool Person::hasValidPhoneNumber() {
    // Les numéros de téléphone valides ont 10 caractères et commentcent par '0'
    if ((this->phoneNumber.length() == 10) && (this->phoneNumber[0] == '0')) {
        /*
        * !!! Attention: si le test est fait dans la négation de cette expression
        * !!! on risque un segmentation fault à l'exécution si la string est vide
        */
        // Vérifier qu'on a bien les 9 autres caractères *numériques*
        for (int i=1; i < 10; ++i) {
            if (isalpha(this->phoneNumber[i])) {
                // on retourne faux au 1er caractère alphabétique !
                return false;
            }
        }
        // Arrivé ici: tout va bien. On retourne vrai !
        return true;
    }
    
    return false; 
}

/* 
* Fonction membre pour valider les prénoms et noms
*/
bool Person::hasValidNames() {
    // Les noms de personne concaténés dans une variable
    string aText = this->firstname + this->lastname;
    if (aText.empty()) {
        // Ni prénom, ni nom
        return false;
    }
    
    // Imposer la locale fr_FR.iso88591 (français), 
    // sous réserve qu'elle soit installée sur le système d'exploitation
    setlocale(LC_ALL, "fr_FR.iso88591");
    
    for (unsigned int i=0; i < aText.length(); ++i) {
        switch (aText[i]) {
            case '-': // accepté les tirets
                continue;
            case '\'': // accepté les apostrophes
                continue;
            case ' ': // accepté les espaces
                continue;
            default:
                if ( !iswalpha((wint_t)aText[i]) ) {
                    // on retourne faux au 1er caractère non alphabétique
                    return false;
                }
        }
    }
    
    return true;
}

/* Fonction membre */
int Person::getNumberOfPositiveResponses() {
    return this->answers.numerator;
}

/* Fonction membre */
int Person::getNumberOfGivenResponses() {
    return this->answers.denominator;
}
