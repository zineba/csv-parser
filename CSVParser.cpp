/*
* CSVParser.cpp
*
*/
#include "CSVParser.hpp"
#include "Person.hpp"
#include <fstream>
#include <iostream>
#include <cstring>

using namespace std;

const char kSEPARATORS[] = ",\n";
const char kSLASH[] = "/";

/* 
* Fonction interne à l'unité de compilation 
*/
vector<string> ParseCells(char* line) {
    char* aCell = 0;
    vector<string> aRow;
    
    // lire la première cellule
    aCell = strtok(line, kSEPARATORS);
    aRow.push_back(aCell);
    
    while (aCell) {
        // lire les cellules restantes
        aCell = strtok(0, kSEPARATORS);
        if (aCell) { // évite de pousser des chaînes vides
            aRow.push_back(aCell);
        }
    }
    
    return aRow;
}

/* 
* Fonction interne à l'unité de compilation 
*/
Person ParsePerson(char* line) {
    char* aCell = 0;
    Person aRow;
    
    // lire la première cellule
    aCell = strtok(line, kSEPARATORS);
    aRow.firstname = aCell;
    // lire la seconde cellule
    aCell = strtok(0, kSEPARATORS);
    aRow.phoneNumber = aCell;
    // lire la troisième cellule: n/d
    aCell = strtok(0, kSLASH); // change le séparateur en /
    aRow.answers.numerator = stoi(aCell); // conversion en int
    aCell = strtok(0, kSEPARATORS);
    aRow.answers.denominator = stoi(aCell); // conversion en int
    // lire la quatrième cellule
    aCell = strtok(0, kSEPARATORS);
    aRow.lastname = aCell;
    
    return aRow;
}

/* Fonction membre */
bool CSVParser::initWithFile(std::string path) {
    this->filepath = path;
    
    ifstream aFile(this->filepath);
    
    if (aFile) {
        string aLine;
        // lire l'en-tête du fichier
        getline(aFile, aLine);
        
        // buffer pour stocker la ligne courante
        char aText[aLine.length() + 1];
        stpcpy(aText, aLine.c_str());
        
        // parser la première ligne
        this->header = ParseCells(aText);
        this->numberOfColumns = this->header.size(); 
        
        // parcourir le fichier ligne par ligne
        while (getline(aFile, aLine)) {
            this->numberOfRows++; // compte le nombre de ligne
            // initialisé à 0 à la déclaration
        }
    }
    else {
        cerr << "Erreur: impossible d'ouvrir le fichier." << endl;
        return false;
    }
    
    return true;
}

/* Fonction membre */
vector<string> CSVParser::getLine(int rowNum) {
    // rowNum compris entre ]0; numberOfRows ]
    if (rowNum == 0 || rowNum > this->numberOfRows) {
        cerr << "Erreur: Numéro de ligne en dehors des limites." << endl;
        return vector<string>(); // tableau vide
    }
    
    ifstream aFile(this->filepath);
    
    if (aFile) {
        string aLine;
        getline(aFile, aLine); // On passe l'en-tête du fichier
        int count = 0;
        
        while (getline(aFile, aLine)) {
            count++;
            if (count == rowNum) {
                // buffer pour stocker la ligne courante
                char aText[aLine.length() + 1];
                stpcpy(aText, aLine.c_str());
                // 
                return ParseCells(aText);
            }
        }
    }
    else {
        cerr << "Erreur: impossible d'ouvrir le fichier." << endl;
    }

    // si on arrive ici, c'est qu'il y a une erreur
    return vector<string>(); // tableau vide
}

/* Fonction membre */
vector<string> CSVParser::getLineWithHighestRateOfResponses() {
    ifstream aFile(this->filepath);
    // variable à retourner
    vector<string> aRow;
    // variables temporaires
    Person pers1;
    Person pers2;
    
    if (aFile) {
        string aLine;
        getline(aFile, aLine); // On passe l'en-tête du fichier
        
        // On lit une première personne
        if (getline(aFile, aLine)) {
            // buffer pour stocker la ligne courante
            char aText[aLine.length() + 1];
            stpcpy(aText, aLine.c_str());
            pers1 = ParsePerson(aText);
        }
        
        // On continue de lire les autres personnes en comparant
        while (getline(aFile, aLine)) {
            // buffer pour stocker la ligne courante
            char aText[aLine.length() + 1];
            stpcpy(aText, aLine.c_str());
            pers2 = ParsePerson(aText);
            if (pers2.answers > pers1.answers) {
                pers1 = pers2; // On garde le plus grand
            }
        }
        
        // Si on est ici, on est sûr d'avoir le plus grand.
        // On remplit donc le tableau de string:
        aRow.push_back(pers1.firstname);
        aRow.push_back(pers1.phoneNumber);
        aRow.push_back(pers1.answers.display());
        aRow.push_back(pers1.lastname);
    }
    else {
        cerr << "Erreur: impossible d'ouvrir le fichier." << endl;
    }
    
    return aRow;
}
