/*
 * FractionTest.cpp
 *
 */

#include "catch.hpp"
#include "Fraction.hpp"

using namespace std;

TEST_CASE("Test fraction display", "[Fraction]") {
    Fraction frac;
    // avec les valeurs par défaut: valide
    REQUIRE( frac.display() == "0/1" );
    // avec dénominateur nulle
    frac.denominator = 0;
    REQUIRE( frac.display() == "" ); // plus erreur dans la console
}

TEST_CASE("Test fraction + entier", "[Fraction]") {
    Fraction frac;
    //
    frac.numerator = 1;
    frac.denominator = 2;
    frac + 1;
    REQUIRE( frac.display() == "3/2" );
    // avec dénominateur nulle
    frac.denominator = 0;
    frac + 1;
    REQUIRE( frac.display() == "" );
}

TEST_CASE("Test fraction + fraction", "[Fraction]") {
    Fraction frac1;
    Fraction frac2;
    frac1 + frac2;
    REQUIRE( frac1.display() == "0/1" );
    
    frac1.denominator = 0;
    frac1 + frac2;
    REQUIRE( frac1.display() == "" );
    
    frac1.numerator = 2;
    frac1.denominator = 3;
    frac2.numerator = 1;
    frac2.denominator = 3;
    frac1 + frac2;
    REQUIRE( frac1.display() == "9/9" );
    
    frac2.denominator = 0;
    frac1 + frac2;
    REQUIRE( frac1.display() == "9/9" );
}

TEST_CASE("Test fraction < fraction", "[Fraction]") {
    Fraction frac1;
    frac1.numerator = 1;
    frac1.denominator = 2;
    // avec les valeurs par défaut
    Fraction frac2;
    REQUIRE( frac2 < frac1 );
    
    // avec dénominateur nulle
    frac1.denominator = 0; // frac1 est devenue invalide
    REQUIRE( frac1 < frac2 ); // par convention
    
    // on change frac1 pour qu'elle soit valide
    frac1.denominator = 3; // 1/3
    // on change frac2
    frac2.numerator = 2;
    frac2.denominator = 0; // frac2 est devenue invalide
    REQUIRE( (frac1 < frac2) == false );
}

TEST_CASE("Test fraction > fraction", "[Fraction]") {
    Fraction frac1;
    frac1.numerator = 1;
    frac1.denominator = 2;
    // avec les valeurs par défaut
    Fraction frac2;
    REQUIRE( frac1 > frac2 );
    
    // avec dénominateur nulle
    frac1.denominator = 0; // frac1 est devenue invalide
    REQUIRE( frac2 > frac1 ); // par convention
    
    // on change frac1 pour qu'elle soit valide
    frac1.denominator = 3; // 1/3
    // on change frac2
    frac2.numerator = 2;
    frac2.denominator = 0; // frac2 est devenue invalide
    REQUIRE( (frac2 > frac1) == false );
}
