/*
* CSVParser.hpp
*
* Note: Lecteur de fichier CSV
*/
#include <string>
#include <vector>

struct CSVParser {
    //@{
    // Attributs membres
    std::string filepath;
    int numberOfColumns = 0;
    int numberOfRows = 0;
    std::vector<std::string> header;
    //@}

    //@{
    // Fonctions membres
    bool initWithFile(std::string path);
    std::vector<std::string> getLine(int rowNum);
    std::vector<std::string> getLineWithHighestRateOfResponses();
    //@}
};

